class TripDuration
  attr_reader :duration_in_seconds, :duration_in_minutes

  def initialize(origin_lat, origin_lon, destination_lat, destination_lon)
    connection = Faraday::Connection.new 'https://router.hereapi.com'
    routes_url = "/v8/routes?apiKey=#{ENV['HERE_API_KEY']}&transportMode=car&origin=#{origin_lat}%2C#{origin_lon}&destination=#{destination_lat}%2C#{destination_lon}&return=summary"
    response = connection.get routes_url
    if response.success?
      json_response = JSON.parse(response.body)
      puts "json_response #{json_response}"
      @duration_in_seconds = json_response['routes'][0]['sections'][0]['summary']['duration']
      @duration_in_minutes = @duration_in_seconds / 60
    else
      raise 'Response Error'
    end
  end
end
