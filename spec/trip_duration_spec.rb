require 'spec_helper'
require 'web_mock'

def when_i_send_trip_request(duration_in_seconds, origin, destination)
  body = { "routes": [
    {
      "id": 'bfaed7d0-19c7-4e72-81b7-24eeb148b62b',
      "sections": [
        {
          "summary": {
            "duration": duration_in_seconds,
            "length": 538
          }
        }
      ]
    }
  ] }

  stub_request(:get, "https://router.hereapi.com/v8/routes?apiKey=#{ENV['HERE_API_KEY']}&transportMode=car&origin=#{origin}&destination=#{destination}&return=summary")
    .to_return(body: body.to_json, status: 200)
end

def when_i_send_invalid_trip_request(_duration_in_seconds, origin, destination)
  body = { "Error": 'Error' }

  stub_request(:get, "https://router.hereapi.com/v8/routes?apiKey=#{ENV['HERE_API_KEY']}&transportMode=car&origin=#{origin}&destination=#{destination}&return=summary")
    .to_return(body: body.to_json, status: 404)
end

describe 'TripDuration' do
  it 'Trip request provides a valid response' do
    when_i_send_trip_request(120, '52.5308,13.3847', '52.5323,13.3789')
    trip = TripDuration.new(52.5308, 13.3847, 52.5323, 13.3789)
    expect(trip.duration_in_seconds).to eq 120
    expect(trip.duration_in_minutes).to eq 2
  end

  it 'Here Api integration test' do
    WebMock.disable!
    trip = TripDuration.new('-34.6060', '-58.4570', '-34.6060', '-58.4570')
    expect { trip.duration_in_minutes }.not_to raise_error('Response Error')
    WebMock.enable!
  end

  it 'Invalid trip request throws an error' do
    when_i_send_invalid_trip_request(120, 'NotLat,13.3847', '52.5323,13.3789')
    expect { TripDuration.new('NotLat', 13.3847, 52.5323, 13.3789) }.to raise_error('Response Error')
  end
end
